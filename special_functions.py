import re
import json

class ExtractNameFunctions():
    name_to_column_number_dict = {
                                  'pubmed_new': 4,
                                  'pubmed_old': 3,
                                  'grants_new': 2,
                                  'grants_old': 1,
                                  'patents_new': 3,
                                  'patents_old': 2,
                                  'apps_new': 3,
                                  'apps_old': 2,
                                  'trials_new': 3,
                                  'trials_old': 2,
                                  'bloomberg_new': 5,
                                  'bloomberg_old': 4,
                                  'public_new': 2,
                                  'public_old': 1,
                                  'ID': 0,
                                  'nasdaq_old': 1,
                                  'nasdaq_new': 9,
                                  'privat_old': 3,
                                  'privat_new': 4,
                                  'linkedin_old': 2,
                                  'linkedon_new': 3,
                                  'data_old': 1,
                                  'data_new': 5, 
                                 }

    def extractFromCSV(self, string, column_number, delimiter='\t'):
        return string.split(delimiter)[column_number].strip().lower()

    def extractCompanyNamePM(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['pubmed_old'])

    def extractCompanyNameAppsPatentsTrials(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['patents_old'])

    def extractCompanyNameGrants(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['grants_old'])

    def extractCompanyNameBloomberg(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['bloomberg_old'])
    
    def extractCompanyNameNasdaq(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['nasdaq_old'])
   
    def extractCompanyNamePrivat(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['privat_old'])
   
    def extractCompanyNameLinkedin(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['linkedin_old'])
    
    def extractCompanyNameData(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['data_old'])

    def extractCompanyPublic(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['public_old'])
    
    def extractCompanyNewNamePM(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['pubmed_new'])

    def extractCompanyNewNameAppsPatentsTrials(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['patents_new']) 

    def extractCompanyNewNameGrants(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['grants_new'])

    def extractCompanyNewNameBloomberg(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['bloomberg_new'])

    def extractCompanyNewNamePublic(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['public_new'])
    
    def extractCompanyNewNameNasdaq(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['nasdaq_new'])
    
    def extractCompanyNewNamePrivat(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['privat_new'])
    
    def extractCompanyNewNameLinkedin(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['linkedin_new'])
    
    def extractCompanyNewNameData(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['data_new'])

    def extractID(self, string):
        return self.extractFromCSV(string, self.name_to_column_number_dict['ID'])


class StringPreparator():
    REPLACE_STRING = r',|\(|\)|\[|\]|\{|\}|\.|\?|\!|\*|\$|&|"|\^|\'|#|-|\/|\\'

    def stringPreparation(string):
        string = string.replace(',', ' ').replace('(', ' ').replace(')', ' ').replace('[', ' ').replace(']', ' ')\
            .replace('{', ' ').replace('}', ' ').replace('.', ' ').replace('?', ' ').replace('!', ' ')\
            .replace('*', ' ').replace('$', ' ').replace('"', ' ').replace('^', ' ')\
            .replace("'", ' ').replace('#', ' ').replace('-', ' ').replace('/', ' ')\
            .replace('\\', ' ').replace('`', ' ').replace('’', ' ').replace('_', ' ').replace('–', ' ').replace(':', ' ')
        string = ' '.join([word for word in string.split() if word != ''])
        return string

    def stringPreparationOne(string):
        string = string.replace(',', ' ').replace('(', ' ').replace(')', ' ').replace('[', ' ').replace(']', ' ')\
            .replace('{', ' ').replace('}', ' ').replace('.', ' ').replace('?', ' ').replace('!', ' ')\
            .replace('*', ' ').replace('$', ' ').replace('"', ' ').replace('^', ' ')\
            .replace("'", ' ').replace('#', ' ').replace('-', ' ').replace('/', ' ')\
            .replace('\\', ' ').replace('`', ' ').replace('’', ' ').replace('_', ' ').replace('–', ' ').replace(':', ' ')
        string = ' '.join([word for word in string.split() if word != ''])
        return string

    def stringPreparationAbbCut(string):
        string = string.replace(',', ' ').replace('(', ' ').replace(')', ' ').replace('[', ' ').replace(']', ' ')\
            .replace('{', ' ').replace('}', ' ').replace('?', ' ').replace('!', ' ')\
            .replace('*', ' ').replace('$', ' ').replace('"', ' ').replace('^', ' ')\
            .replace("'", ' ').replace('#', ' ').replace('-', ' ').replace('/', ' ')\
            .replace('\\', ' ').replace('`', ' ').replace('’', ' ').replace('_', ' ').replace('–', ' ').replace(':', ' ')
        string = ' '.join([word for word in string.split() if word != ''])
        return string

    def delEmail(string):
        no_email_string = re.sub(r'\s[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+\s?', '', string)
        return no_email_string

    

class FileOptions():
    def extractPatStartEnd(self, company_name, struct):
        pattern = list(struct.start_end_dict[company_name].keys())[0]
        start_index = struct.start_end_dict[company_name][pattern]['start']
        end_index = struct.start_end_dict[company_name][pattern]['end']
        return pattern, start_index, end_index

    def jsonDumpFile(self, entry, file_name, folder):
        file_output = open('{0}{1}'.format(folder, file_name), 'w+')
        file_output.write(json.dumps(entry, sort_keys=True, indent=4))
        file_output.close()

    def printNewIndex(self, entity, company_name, company_string, file_output, type_company):
        pattern, start_index, end_index = self.extractPatStartEnd(company_name, entity)
        file_output.write('{0}\t{1}\t{2}\t{3}\t{4}\n'.format('\t'.join(company_string.split('\t')).strip(),
                                                             pattern, start_index, end_index,
                                                             type_company))

    def printNewIndexLite(self, company_string, company_name, pattern, file_output, type_company):
        start_index = company_name.find(pattern)
        if start_index == -1:
            start_index = 'nothing'
            end_index = 'nothing'
        else:
            end_index = start_index + len(pattern)
        file_output.write('{0}\t{1}\t{2}\t{3}\t{4}\n'.format('\t'.join(company_string.strip().split('\t')),
                                                             pattern, start_index, end_index,
                                                             type_company))

   
