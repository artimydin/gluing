from special_functions import ExtractNameFunctions
import os

#<<<<<<< HEAD
#VERSION = '15'
FOLDER = '/home/zav/gluing/'
#=======
VERSION = '17'
#FOLDER = os.path.dirname(os.path.abspath(__file__)) + '/'
#>>>>>>> 70f77548a22b4580ab098f7ee4dd4d506b13668a

ENTITY_DICT = { 
                'pubmed': {
                           'result_file': '{0}pubmed/new_index_file_stage_2_v{1}'.format(FOLDER, VERSION),
                           'result_stage_1_file': '{0}pubmed/new_index_file_stage_1_v{1}'.format(FOLDER, VERSION), 
                           'extract_old_name_function': ExtractNameFunctions().extractCompanyNamePM, 
                           'extract_new_name_function': ExtractNameFunctions().extractCompanyNewNamePM,
                           'source_file': '/home/aad/maria/reduce_coms/patterns/Stage_1/Non_commercial/source/pm_rg_joined4.csv',
                           'patterns_file': '{0}{1}'.format('', 'patterns_pubmed'),
                           'result_folder': 'pubmed/', 
                          },

                'patents': {
                            'result_file': '{0}patents/new_index_file_v{1}'.format(FOLDER, VERSION),                           
                            'result_stage_1_file': '{0}patents/new_index_file_v{1}'.format(FOLDER, VERSION),
                            'extract_old_name_function': ExtractNameFunctions().extractCompanyNameAppsPatentsTrials, 
                            'extract_new_name_function': ExtractNameFunctions().extractCompanyNewNameAppsPatentsTrials,
                            'source_file': '/home/aad/maria/reduce_coms/patterns/Stage_1/Non_commercial/source/gluing/patent_assoc_com',
                            'patterns_file': '{0}{1}'.format('', 'patterns'),
                            'result_folder': 'patents/',
                           },

                'apps': {
                         'result_file': '{0}apps/new_index_file_v{1}'.format(FOLDER, VERSION),                            
                         'result_stage_1_file': '{0}apps/new_index_file_v{1}'.format(FOLDER, VERSION),
                         'extract_old_name_function': ExtractNameFunctions().extractCompanyNameAppsPatentsTrials,
                         'extract_new_name_function': ExtractNameFunctions().extractCompanyNewNameAppsPatentsTrials,
                         'result_folder': 'apps/',
                         'source_file': '/home/aad/maria/reduce_coms/patterns/Stage_1/Non_commercial/source/gluing/apps_assoc_com',
                         'patterns_file': '{0}{1}'.format('', 'patterns'),
                        },

                'grants': {
                           'result_file': '{0}grants/new_index_file_v{1}'.format(FOLDER, VERSION),            
                           'result_stage_1_file': '{0}grants/new_index_file_v{1}'.format(FOLDER, VERSION),
                           'extract_old_name_function': ExtractNameFunctions().extractCompanyNameGrants,
                           'extract_new_name_function': ExtractNameFunctions().extractCompanyNewNameGrants,
                           'source_file': '/home/aad/maria/reduce_coms/patterns/Stage_1/Non_commercial/source/gluing/nih_association_com',
                           'patterns_file': '{0}{1}'.format('', 'patterns'), 
                           'result_folder': 'grants/', 
                          },

                'trials': {
                           'result_file': '{0}trials/new_index_file_v{1}'.format(FOLDER, VERSION),
                           'result_stage_1_file': '{0}trials/new_index_file_v{1}'.format(FOLDER, VERSION),
                           'extract_old_name_function': ExtractNameFunctions().extractCompanyNameAppsPatentsTrials, 
                           'extract_new_name_function': ExtractNameFunctions().extractCompanyNewNameAppsPatentsTrials,
                           'result_folder': 'trials/',
                           'source_file': '/home/aad/maria/reduce_coms/patterns/Stage_1/Non_commercial/source/gluing/trials_association_com',
                           'patterns_file': '{0}{1}'.format('', 'patterns'),
                          },

                'bloomberg': {
                              'result_file': '{0}bloomberg/bloomberg_gluing_results_v{1}'.format(FOLDER, VERSION),
                              'result_stage_1_file': '{0}bloomberg/bloomberg_gluing_results_v{1}'.format(FOLDER, VERSION),
                              'extract_old_name_function': ExtractNameFunctions().extractCompanyNameBloomberg, 
                              'extract_new_name_function': ExtractNameFunctions().extractCompanyNewNameBloomberg,
                              'result_folder': 'bloomberg/',
                              'source_file': '/home/aad/maria/reduce_coms/patterns/Stage_1/Non_commercial_debug_v2/bloomberg/data_privat.csv',
                              'patterns_file': '{0}{1}'.format('', 'patterns')
                             },
                'public': {
                           'result_file': '{0}public_company/public_gluing_results_v{1}'.format(FOLDER, VERSION),
                           'result_stage_1_file': '{0}public_company/public_gluing_results_v{1}'.format(FOLDER, VERSION),
                           'extract_old_name_function': ExtractNameFunctions().extractCompanyPublic, 
                           'extract_new_name_function': ExtractNameFunctions().extractCompanyNewNamePublic,
                           'result_folder': 'public_company/',
                           'source_file': '/tmp/public_company',
                           'patterns_file': '{0}{1}'.format('', 'patterns'), 
                          },

                'nasdaq': {
                           'result_file': '/home/aad/maria/reduce_coms/patterns/Stage_1/Non_commercial_debug_v2/nasdaq/',
                           'extract_old_name_function': '',
                           'extract_new_name_function': '',
                           'result_folder': '',
                           'source_file': '',
                           'patterns_file': '',
                          },
     
                'debug_apps': {
                         'result_file': '{0}apps/apps_gluing_results_v{1}'.format(FOLDER, VERSION),                            
                         'result_stage_1_file': '{0}apps/apps_gluing_results_v{1}'.format(FOLDER, VERSION),
                         'extract_old_name_function': ExtractNameFunctions().extractCompanyNameAppsPatentsTrials,
                         'extract_new_name_function': ExtractNameFunctions().extractCompanyNewNameAppsPatentsTrials,
                         'result_folder': 'apps/',
                         'source_file': '/home/aad/maria/reduce_coms/patterns/Stage_1/Non_commercial_debug_v2/apps/spinefrontier_lls.csv',
                         'patterns_file': '{0}{1}'.format('', 'patterns'),
                          },

                'debug_patents': {
                            'result_file': '/home/aad/maria/reduce_coms/patterns/Stage_1/Non_commercial_debug_v2/patents/current_new_index_debug',
                            'extract_old_name_function': ExtractNameFunctions().extractCompanyNameAppsPatentsTrials,
                            'extract_new_name_function': ExtractNameFunctions().extractCompanyNewNameAppsPatentsTrials,
                            'source_file': '/home/aad/maria/reduce_coms/patterns/Stage_1/Non_commercial_debug_v2/patents/wake_forest.csv',
                            'patterns_file': '{0}{1}'.format('', 'patterns'),
                            'result_folder': 'patents/',
                           },

                'debug_articles': {
                            'result_file': '/home/aad/maria/reduce_coms/patterns/Stage_1/Non_commercial_debug_v2/patents/current_new_index_debug',
                            'extract_old_name_function': ExtractNameFunctions().extractCompanyNamePM,
                            'extract_new_name_function': ExtractNameFunctions().extractCompanyNewNamePM,
                            'source_file': '/home/aad/maria/reduce_coms/patterns/Stage_1/Non_commercial_debug_v2/pubmed/agfa.csv',
                            'patterns_file': '{0}{1}'.format('', 'patterns_pubmed'),
                            'result_folder': 'pubmed/',
                           }
}
