import os
import json
import sys

from gluing_configure import ENTITY_DICT

if __name__ == '__main__':
    gluing_file_name = ENTITY_DICT['pubmed']['result_stage_1_file']
    new_gluing_file_name = ENTITY_DICT['pubmed']['result_file']

    new_rules = json.load(open('new_rules_gluing_v5.json'))
    new_rules_reverse = json.load(open('new_rules_gluing_v7_reverse.json'))

    gluing_file = open(gluing_file_name)
    new_gluing_file = open(new_gluing_file_name, 'w+')
    
    count, old_count = 0, 0 
    for string in gluing_file:
        if len(string.split('\t')) < 5:
            new_gluing_file.write(string)
            continue
        count += 1
        if int(count / int(sys.argv[1])) != old_count:
            print(count)
            old_count += 1
        split_list = string.split('\t')
        ref_name, old_name = split_list[3].strip(), split_list[4].strip()
        if old_name in new_rules_reverse:
            for new_name in new_rules_reverse[old_name]:
                begin_string, end_string = '\t'.join(string.split('\t')[:4]), '\t'.join(string.split('\t')[5:]).strip()
                new_gluing_file.write('{0}\t{1}\t{2}\n'.format(begin_string, new_name, end_string))
                break
        else:
            new_gluing_file.write(string)

    gluing_file.close()
    new_gluing_file.close()
