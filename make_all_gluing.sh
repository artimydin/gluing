#!/bin/bash

max_num=10000000000

#python3 make_gluing_stage_1.py pubmed $1 $max_num
#python3 make_gluing_stage_1.py patents $1 $max_num
#python3 make_gluing_stage_1.py apps $1 $max_num
#python3 make_gluing_stage_1.py trials $1 $max_num
#python3 make_gluing_stage_1.py grants $1 $max_num
#python3 make_gluing_stage_1.py public $1 $max_num
#python3 make_gluing_stage_1.py bloomberg $1 $max_num

python3 make_gluing_stage_2.py $1
#
python3 make_company_statistics_v2.py $1 pubmed patents apps grants trials
#
#python send_gluing_email.py
