import re
import json
import extract_pattern_functions
from gluing_algorithm import ABBCompanyPatterns

class ExtractEmail():
    def findEmail(self, string):
        return [word for word in re.split(',|;| ', string) if '@' in word]

    def delEmail(self, string):
        res_string = string
        for word in self.findEmail(string):
            res_string = res_string.replace(word, ', email ,')
        #print(res_string)
        return res_string

class ExtractAddress():
    def __init__(self):
        self.one_word_countries = json.load(open('contries_one_word_set'))
        self.two_word_countries = json.load(open('contries_two_word_set'))
        self.country_accordance = json.load(open('country_accordance_file'))
        self.univ_templ_set = set(string.split(':')[0] for string in open('univ_templ_file'))
     
    def normaliseName(self, string):
        return self.country_accordance[string]

    def findCountry(self, string, split_string=' '):
        res_list = list(set(self.one_word_countries).intersection(re.split(split_string, string.lower())))
        res_list += [name for name in set(self.two_word_countries) if name in string.lower()]
        res_list = [self.normaliseName(name) for name in res_list]
        return res_list

    def findCity(self, string, country):
        pass

    def isUnivTemplInString(self, string, split_string=' '):
        if set(string.lower().split(' ')).intersection(self.univ_templ_set):
            return True
        else:
            return False

    def findAddress(self, string, split_string='\.|,|;'):
        string_list = re.split(split_string, string)
        res_list = []
        for pattern in string_list:
            if not self.isUnivTemplInString(pattern):
                if not ABBCompanyPatterns().isCorpAbbInString(pattern):
                    country_list = self.findCountry(pattern)
                    if country_list:
                        res_list += [{pattern: country_list}]
        return res_list
        
    def makeAddressDict(self, string_list):
        res_dict = {}
        for string in string_list:
            #print(ExtractEmail().delEmail(string))
            res_dict[ExtractEmail().delEmail(string)] = self.findAddress(ExtractEmail().delEmail(string))
        return res_dict

    def divideByPattern(self, string, pattern):
        tmp_pattern = ''
        for index in range(len(string)):
            if string[index] in {'.', ',', ';'}:
                #print('^', tmp_pattern)
                if tmp_pattern == pattern:
                    return string[:index - len(pattern)], string[index:]
                tmp_pattern = ''
            else:
                tmp_pattern += string[index]
        return (string)
        #print('$', string)
        #print('$', pattern)
             
    def divideByCountry(self, string, country_list):
        res_list, tmp_string = [], string
        #print('#', string)
        #print('#', country_list)
        if country_list:
            for pattern_dict in country_list:
                for pattern in pattern_dict:
                    dividen_list = self.divideByPattern(tmp_string, pattern)
                    #print(dividen_list)
                    res_list.append(dividen_list[0])
                    try:
                        tmp_string = dividen_list[1]
                    except IndexError:
                        break
            return res_list 
        else:
            return [string]

    def divideListByCountry(self, company_dict):
        res_dict = {}
        for name in company_dict:
            res_dict[name] = self.divideByCountry(name, company_dict[name])
        return res_dict
    
    
