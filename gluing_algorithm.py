# coding: utf-8
import re
import json

from gluing_configure import FOLDER
from Levenshtein import distance
from special_functions import StringPreparator

stringPreparation = StringPreparator.stringPreparation
stringPreparationOne = StringPreparator.stringPreparationOne


class AcademicPatterns():
    def __init__(self):
        BAD_WORDS_FILE_NAME = 'bad_words'
        SPECIAL_WORDS_FILE_NAME = 'second_univ_words'
        TEMPL_LIST_FILE_NAME = 'univ_templ_file'
        LOCAL_FOLDER = 'academic_patterns/'
        
        self.bad_words_set = {string.strip() 
                              for string in open('{0}{1}{2}'.format(FOLDER, LOCAL_FOLDER, BAD_WORDS_FILE_NAME))}
        self.special_words = {string.strip().lower() 
                              for string in open('{0}{1}{2}'.format(FOLDER, LOCAL_FOLDER, SPECIAL_WORDS_FILE_NAME))}
        self.templ_list = [(string.split(':')[0], string.split(":")[1].strip()) 
                      for string in open('{0}{1}{2}'.format(FOLDER, LOCAL_FOLDER, TEMPL_LIST_FILE_NAME), 'r').readlines()]

    def isInSpecialWords(self, string):
        special_words = self.special_words
        if string in special_words:
            return True
        elif len([True for word in special_words  if distance(string, word) <= len(word) * 0.1]) > 0:
            return True
        return False

    def genAdvWords(self, string_list):
        res_list = []
        flag = False
        bad_set = set(['at', 'of', 'for', 'in', 'de', 'di'])
        for word in string_list:
            res_list.append(word.strip())
            if not self.isInSpecialWords(word.strip()) and len(res_list) >= 1 and word.strip() not in bad_set:
                flag = True
        if flag:
            return ' '.join(res_list)
        else:
            return 'False' + ' '.join(res_list)

    def extractTemplWord(self, string, templ, direct='after'):
        templ_pos = string.find(templ)
        if templ_pos != -1:
            string = [stringPreparation(pattern) for pattern in re.split(',|;| - ', string) if templ in stringPreparation(pattern)][0].strip()
            string_list = string.split(templ)
            left_word = ' '.join(reversed(self.genAdvWords(reversed(string_list[0].split())).split()))
            right_word = self.genAdvWords(string_list[1].split())
            #print direct, left_word, right_word
            if 'False' in left_word or 'False' in right_word or len(left_word) < 2 or len(right_word) < 2:
                left_word, right_word = left_word.replace('False', '').strip(), right_word.replace('False', '').strip()
                direct = 'after and before'
            if direct == 'after':
                return '{0} {1}'.format(templ, right_word).strip()
            elif direct == 'after and before':
                return '{0} {1} {2}'.format(left_word, templ, right_word).strip()
            elif direct == 'before':
                return '{0} {1}'.format(left_word, templ).strip()
        else:
            return False

    def extractTempl(self, string):
        for templ in self.templ_list:
            res = self.extractTemplWord(string, templ[0], templ[1])
            if res:
                return res
            else:
                False
    
    def returnNewNameList(self, string):
        new_name = self.extractTempl(string)
        if new_name:
            return [new_name.strip()]
        else:
            return False


class CityPatterns():
    def __init__(self):
        LOCAL_FOLDER = 'city_patterns/'
        CITIES_FILE_NAME = 'cities.txt'

        self.cities_set = set([stringPreparation(word).lower() 
                          for word in json.load(open('{0}{1}{2}'.format(FOLDER, LOCAL_FOLDER, CITIES_FILE_NAME)))])

    def returnNewNameList(self, string):
        comp_cities = set(stringPreparation(string).split()) & self.cities_set
        #print(comp_cities)
        # если не нашли городов в строке возвращаем исходную строку
        if not comp_cities:
            return False

        # разделяем строку на части по запятым
        company_parts = string.split(',')
        # ищем справа крайнюю правую часть, в которой есть город, после обрежем строку до нее
        right_part = len(company_parts)   
        for city in comp_cities:
            # перебираем все части строки справа налево
            for i, part in reversed(list(enumerate(company_parts))):
                part = stringPreparation(part)
                if city in part:
                    # если в части есть город и она не первая, то она - крайняя правая
                    if right_part > i and i != 0:
                        right_part = i
        # если нашли город только в первой части строки или первая часть строки пустая возвращаем исходную строку
        if right_part == len(company_parts) or right_part == 1 and company_parts[0] == '':
            return False
        new_company_name = ' '.join(map(lambda part: stringPreparation(part), company_parts[:right_part]))
        return [new_company_name.strip()]


class ABBCompanyPatterns():
    def __init__(self):
        LOCAL_FOLDER = 'abb_com_cut/'
        CORP_ABB_FILE_NAME = 'Corp_abbreviation'

        self.corp_abb_set = set([string.strip()
                            for string in open('{0}{1}{2}'.format(FOLDER, LOCAL_FOLDER, CORP_ABB_FILE_NAME), 'r') if string.strip() != ''])
        #print(self.corp_abb_set)

#    def isCorpAbbInString(self, string):
#        if set(stringPreparation(string).split()).intersection(self.corp_abb_set):
#            return True
#        else:
#            return False

#    def delCorpAbb(self, string):
#        return ' '.join([word for word in string.split() if word not in self.corp_abb_set])

    def findAbbinString(self, string, abb):
        if '.' in abb or ' ' in abb or '/' in abb:
            #print('#', string, abb)
            if abb in string:
                return True
        else:
            #print('%', abb, stringPreparation(string).split())
            if abb in stringPreparation(string).split():
                return True
        return False

    def splitStringByAbb(self, string, abb):
         if '.' in abb or ' ' in abb or '/' in abb:
             #print('##', abb, string, string.split(abb, 1))
             return string.split(abb, 1)[0]
         else:
             #print(abb)
             #print('%%', abb, stringPreparation(string), stringPreparation(string).split(abb)[0])
             return re.split(' {0} | {0}$'.format(abb), stringPreparation(string))[0]

    def findCorpPattern(self, string):
        pattern = None
        for abb in self.corp_abb_set:
            if self.findAbbinString(string, abb):
                pattern = self.splitStringByAbb(string, abb)
                if pattern:
                    string = pattern
        if pattern:
            return stringPreparation(pattern)
        return False

    def returnNewNameList(self, string):
        #company_name = stringPreparation(string)
        pattern = self.findCorpPattern(string)
        #if self.isCorpAbbInString(company_name) and self.delCorpAbb(company_name):
        if pattern:
            return [pattern.strip()]
        else:
            return False


class PatternsTop1000():
    def __init__(self, patterns_file_name):
        LOCAL_FOLDER = 'pattern_TOP_1000/'
        STOP_WORDS_FILE_NAME = 'patterns_stop_words'
        

        self.stop_words_set = set(word.strip() 
                                  for word in open('{0}{1}{2}'.format(FOLDER, LOCAL_FOLDER, STOP_WORDS_FILE_NAME)))
        self.patterns_dict = {string.split(',')[0].strip().lower():
                    [word.strip().lower() for word in string.lower().split(',') if word.strip() != '']
                    for string in open('{0}{1}{2}'.format(FOLDER, LOCAL_FOLDER, patterns_file_name)) if string.strip() != ''}
        self.main_patterns_dict = {pattern : main_pattern for main_pattern in self.patterns_dict
                                                for pattern in self.patterns_dict[main_pattern]}
        self.patterns_set = set(pattern for main_pattern in self.patterns_dict
                        for pattern in self.patterns_dict[main_pattern])

    def delStopWords(self, string):
        res_string = string
        for word in self.stop_words_set:
            res_string = res_string.replace(word, '')
        return res_string

    def makeStringSet(self, string):
        #print colored(string.split(), 'blue')
        string_list = string.split()
        return set(' '.join(string_list[i:j]) for i in range(len(string_list)) for j in range(i + 1, len(string_list) + 1))

    def findPattern(self, string):
        company_name = stringPreparationOne(string)
        company_set = self.makeStringSet(self.delStopWords(company_name))
        res_set = set()
        for pattern in self.patterns_set.intersection(company_set):
            res_set.update({self.main_patterns_dict[pattern]})
        return res_set

    def returnNewNameList(self, string):
        patterns_set = self.findPattern(string)
        if patterns_set:
           return patterns_set
        else:
            return False


