import re
import json
import sys

from gluing_configure import ENTITY_DICT, FOLDER, VERSION
from special_functions import FileOptions, StringPreparator
from gluing_algorithm import AcademicPatterns, CityPatterns, ABBCompanyPatterns, PatternsTop1000

DICT_BAD_NEW_NAMES = {'geneva switzerland': lambda string: StringPreparator.stringPreparation(string).strip().strip('geneva switzerland')}

def printLog(count, old_count, count_difference, text):
    tmp_count, tmp_old_count = count, old_count
    tmp_count += 1
    if tmp_count - tmp_old_count >= count_difference:
        tmp_old_count = tmp_count
        print(text.format(count))
    return tmp_count, tmp_old_count


def extractCompany(string, extractFunction):
    try:
        company_name_row = extractFunction(string).lower().strip()
    except Exception:
        return False

    if '@' in company_name_row:
        company_name_row = StringPreparator.delEmail(company_name_row)
    return company_name_row


def makeGluingIteration(company_name_row, algorithm_dict):
    for tmp_dict in algorithm_dict:
        for name in tmp_dict:
            pattern_list = tmp_dict[name].returnNewNameList(company_name_row)
            if pattern_list:
                return pattern_list, name
    company_name_row = StringPreparator.stringPreparation(company_name_row)
    return [company_name_row], 'nothing'


def makeGluingStageOne(gluing_file, algorithm_dict, extractFunction, count_difference, max_count, new_index_file, text):
    file_option_object = FileOptions()
    count, old_count = 0, 0
    for string in open(gluing_file):
        count, old_count = printLog(count, old_count, count_difference, text)
        if count > max_count:
            break

        company_name_row = extractCompany(string, extractFunction)
        if not company_name_row:
            new_index_file.write('{0}\t{1}\t{2}\t{3}\t{4}\n'.format(string.strip(), 'nothing', 'nothing', 'nothing', 'nothing'))
            continue

        pattern_list, name = makeGluingIteration(company_name_row, algorithm_dict)
        for pattern in pattern_list:
            for bad_name, function in DICT_BAD_NEW_NAMES.items():
                if bad_name in pattern:
                    pattern = function(company_name_row)
            file_option_object.printNewIndexLite(string, company_name_row, pattern, new_index_file, name)


if __name__ == '__main__':
    if len(sys.argv) == 4:
        word = sys.argv[1]
        count_difference = int(sys.argv[2])
        max_count = int(sys.argv[3])
        entity = ENTITY_DICT[sys.argv[1]]
        extractCompanyName = entity['extract_old_name_function']
        result_folder = entity['result_folder']
        source_file = entity['source_file']
        patterns_file_name = entity['patterns_file']
        result_file = entity['result_stage_1_file']
        version = VERSION
    else:
        extractCompanyName, result_folder, source_file = sys.argv[1:]
    
    algorithm_dict = [
                       {
                        'pattern_TOP_1000': PatternsTop1000(patterns_file_name)
                       },
                       {
                        'academic': AcademicPatterns()
                       },
                       {
                        'llc_ltd_cut': ABBCompanyPatterns()
                       },
                       {
                        'cities_cut': CityPatterns()
                       },
                     ]

    new_index_file = open(result_file, 'w+')
    text = 'In ' + word + ' were glued {0} in stage 1'
    makeGluingStageOne(source_file, algorithm_dict, extractCompanyName, count_difference, max_count, new_index_file, text)
