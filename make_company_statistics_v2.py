import json
import sys

from gluing_configure import ENTITY_DICT, VERSION
from special_functions import ExtractNameFunctions

extractID = ExtractNameFunctions().extractID


class General(Exception):
    pass


class BadEntity(General):
    def __str__(self, value):
        return 'Entity must be in set ("app", "patent", "article", "trial", "grant")'


class CompanyEntity():
    def __init__(self, name, articles=set(), trials=set(), patents=set(), apps=set(), grants=set()):
        self.name = name
        self.articles = set(articles)
        self.trials = set(trials)
        self.patents = set(patents)
        self.apps = set(apps)
        self.grants= set(grants)
	
    def addPatent(self, patent):
        self.patents.update([patent])
 
    def addTrial(self, trial):
        self.trials.update([trial])
   
    def addApp(self, app):
        self.apps.update([app])
   
    def addGrant(self, grant):
        self.grants.update([grant])

    def addArticle(self, article):
        self.articles.update([article])

    def addEntity(self, value, entity):
        if entity == 'patents':
            self.addPatent(value)
        elif entity == 'apps':
            self.addApp(value)
        elif entity == 'grants':
            self.addGrant(value)
        elif entity == 'pubmed':
            self.addArticle(value)
        elif entity == 'trials':
            self.addTrial(value)
        else:
            print(entity)
            raise BadEntity(entity)

    def __str__(self):
        return '{0}\t{1}\t{2}\t{3}\t{4}\t{5}'.format(self.name, len(self.patents),
                                                     len(self.apps), len(self.grants),
                                                     len(self.articles), len(self.trials))


class CompanyDict():
    def __init__(self):
        self.company_dict = {}

    def addCompany(self, name, value=None, entity=None):
        #if name == 'egarsat suma intermutual barcelona':
        #      print(value, entity)
        #      raise IndexError
        company = CompanyEntity(name)
        if name not in self.company_dict: 
            company.addEntity(value, entity)
            self.company_dict.update({name: company})
        elif value != None and entity != None:
            company = self.company_dict[name]
            company.addEntity(value, entity)
        else:
            raise BadEntity(entity)
    
    def __str__(self):
        return '\n'.join([str(company) for company in self.company_dict.values()])



if __name__ == '__main__':
    count_difference = int(sys.argv[1])

    main_company_dict = CompanyDict()
    for word in sys.argv[2:]:
        count, count_old = 0, 0
        if word in ENTITY_DICT:
            entry = open(ENTITY_DICT[word]['result_file']).readlines()
            extractCompanyName = ENTITY_DICT[word]['extract_new_name_function']
            for string in entry:
                count += 1
                if count - count_old >= count_difference:
                    print(word, count)
                    count_old = count
                try:
                    company_name = extractCompanyName(string)
                except IndexError:
                    continue
                if company_name == '':
                    continue
                entity_id = extractID(string)
                main_company_dict.addCompany(company_name, value=entity_id, entity=word) 
    open('statistic_test', 'w+').write(json.dumps({name: {'articles': list(main_company_dict.company_dict[name].articles),
                                                          'patents': list(main_company_dict.company_dict[name].patents),
                                                          'trials': list(main_company_dict.company_dict[name].trials),
                                                          'apps': list(main_company_dict.company_dict[name].apps),
                                                          'grantes': list(main_company_dict.company_dict[name].grants) 
                                                          }for name in main_company_dict.company_dict}, indent=4))
    open('statistics/full_statistics_v{0}.csv'.format(VERSION), 'w+').write(str(main_company_dict))
